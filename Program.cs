﻿using System;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Security.Cryptography.Pkcs;

namespace signdata
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            // Проверка корректности переданных параметров.
            if (args.Length < 1)
            {
                Console.WriteLine("Требуется серийный номер сертификата");
                return;
            }

            String signerName = args[0];

            // Исходное сообщение.
            String msg = File.ReadAllText("req.txt");

            Console.WriteLine("{0}Исходное сообщение (длина {1}): {2}  ",
                Environment.NewLine, msg.Length, msg);

            // Переводим исходное сообщение в массив байтов.
            Encoding unicode = Encoding.Unicode;
            byte[] msgBytes = unicode.GetBytes(msg);

            Console.WriteLine("{0}{0}------------------------------",
                Environment.NewLine);
            Console.WriteLine(" Поиск сертификата            ");
            Console.WriteLine("------------------------------{0}",
                Environment.NewLine);

            // Получаем сертификат ключа подписи;
            // он будет использоваться для получения 
            // секретного ключа подписи.
            X509Certificate2 signerCert = GetSignerCert(signerName);

            byte[] encodedSignedCms = SignMsg(msgBytes, signerCert);

            try
            {
                using (StreamWriter sw = new StreamWriter("lastsign.txt", false))
                {
                    sw.WriteLine(Convert.ToBase64String(encodedSignedCms));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        // Открываем хранилище 'My' и ищем сертификат
        // для подписи сообщения. 
        static X509Certificate2 GetSignerCert(string signerName)
        {
            // Открываем хранилище My.
            X509Store storeMy = new X509Store(StoreName.My,
                StoreLocation.CurrentUser);
            storeMy.Open(OpenFlags.ReadOnly);


            // Ищем сертификат для подписи.
            X509Certificate2Collection certColl =
                storeMy.Certificates.Find(X509FindType.FindBySerialNumber, signerName, false);
            Console.WriteLine(
                "Найдено {0} сертификат(ов) в хранилище {1} для субъекта {2}",
                certColl.Count, storeMy.Name, signerName);

            // Проверяем, что нашли требуемый сертификат
            if (certColl.Count == 0)
            {
                Console.WriteLine(
                    "Сертификат для данного примера не найден " +
                    "в хранилище. Выберите другой сертификат для подписи. ");
                return null;
            }

            storeMy.Close();


            return certColl[0];
        }

        // Подписываем сообщение секретным ключем.
        static byte[] SignMsg(
            Byte[] msg,
            X509Certificate2 signerCert)
        {
            // Создаем объект ContentInfo по сообщению.
            // Это необходимо для создания объекта SignedCms.
            ContentInfo contentInfo = new ContentInfo(msg);

            // Создаем объект SignedCms по только что созданному
            // объекту ContentInfo.
            // SubjectIdentifierType установлен по умолчанию в 
            // IssuerAndSerialNumber.
            // Свойство Detached установлено по умолчанию в false,
            // Меняем на TRUE
            SignedCms signedCms = new SignedCms(contentInfo, true);

            // Определяем подписывающего, объектом CmsSigner.
            CmsSigner cmsSigner = new CmsSigner(signerCert);

            cmsSigner.IncludeOption = X509IncludeOption.EndCertOnly;

            // Подписываем CMS/PKCS #7 сообение.
            Console.Write("Вычисляем подпись сообщения для субъекта " +
                "{0} ... ", signerCert.SubjectName.Name);
            signedCms.ComputeSignature(cmsSigner);
            Console.WriteLine("Успешно.");

            // Кодируем CMS/PKCS #7 сообщение.
            return signedCms.Encode();
        }


    }
}
